def UNet(n_input_channels,
         n_output_channels,
         depth,
         max_filters,
         batchnorm=False,
         dropout=0.1,
         residual_blocks=False,
         residual_connection=False):
    '''
    Generate network with UNet architecture with specified depth.
    UNet is an autoencoder-like model with skip connections.

    Ref: Ronneberger, Fischer, Brox (2015), U-Net: Convolutional Networks for Biomedical Image Segmentation, MICCAI, LNCS, Vol.9351
    https://arxiv.org/abs/1505.04597

    Args:
    - n_input_channels (int): number of input channels
    - n_output_channels (int): number of output channels
    - depth (int): depth of "U", i.e. network size, measured in number of skip connections
    - max_filters (int): number of filters at bottom layer, power of 2
    - batchnorm (bool): if true, add batch normalization layer
    - dropout (float): if > 0.0, add dropout layer with this dropout rate
    - residual_blocks (bool): if true, add a residual connection over each convolution block
    - residual_connection (bool): if true, add a residual connection from input to output layer (requires n_input_channels==n_output_channels)

    Returns:
    - keras model
    '''

    from keras import layers, models
    import keras.backend as K
    def Conv2DReluBatchNorm(x, n_filters, kernel_size, strides, batchnorm=False, dropout=0., name='', activation='relu', last_layer=False):
        shortcut = x

        if batchnorm:
            x = layers.BatchNormalization()(x)
        x = layers.Conv2D(n_filters, kernel_size,
                          strides=strides, padding='same',
                          name=name)(x)
        x = layers.Activation(activation)(x)
        if dropout > 0:
            x = layers.Dropout(dropout)(x)

        if residual_blocks and not last_layer:
            if K.int_shape(x) != K.int_shape(shortcut): # strides != (1,1):
                # if dimensionality of
                shortcut = layers.Conv2D(n_filters, kernel_size=(1,1), strides=strides, padding='same')(shortcut)
                shortcut = layers.BatchNormalization()(shortcut)

            x = layers.add([shortcut, x])

        return x


    i = layers.Input(shape=(None, None, n_input_channels))

    filters_per_layer_up = [max_filters//(2**i) for i in range(depth+1) ]
    filters_per_layer_down = filters_per_layer_up[::-1]
    filters_per_layer_up.pop() # do not repeat n_filters for bottom layer

    ### downsampling path
    downsampling_layers = []
    for d in range(depth+1):
        if d == 0:
            x = i
            kernel = (1,1)
            strides = (1,1)
        else:
            kernel = (3,3)
            strides = (2,2) # downsampling per stride instead of maxpool
        #print(filters_per_layer_down[d])
        x = Conv2DReluBatchNorm(x, n_filters=filters_per_layer_down[d],
                                kernel_size=kernel, strides=strides,
                                batchnorm=batchnorm, dropout=dropout, name='down_{}'.format(d))
        downsampling_layers.append(x)


    # upsampling path
    bottom_layer = downsampling_layers.pop()
    upsampling_layers = [bottom_layer]

    for d in range(depth):

        u = layers.UpSampling2D(size=(2,2))(upsampling_layers.pop()) # upsample last layer
        c = layers.Concatenate(axis=-1)([u, downsampling_layers.pop()]) # concatenate with corresponding layer in downsampling path
        x = Conv2DReluBatchNorm(c, n_filters=filters_per_layer_up[d],
                                kernel_size=kernel, strides=(1,1), ## STRIDES
                                batchnorm=batchnorm, dropout=dropout, name='up_{}'.format(d))

        upsampling_layers.append(x)

    # output
    o = Conv2DReluBatchNorm(upsampling_layers[-1], n_filters=n_output_channels, kernel_size=(1,1), strides=(1,1), activation='sigmoid', last_layer=True)


    if residual_connection:
        if K.int_shape(i) != K.int_shape(o): # strides != (1,1):
            # if dimensionality of
            shortcut = layers.Conv2D(n_output_channels, kernel_size=(1,1), strides=(1,1), padding='same')(i)
            shortcut = layers.BatchNormalization()(shortcut)

        #assert n_input_channels == n_output_channels, "Cannot make residual connection: input and output must have same number of channels."
        o = layers.add([shortcut,o])
        o = Conv2DReluBatchNorm(o, n_filters=n_output_channels, kernel_size=(1,1), strides=(1,1), activation='sigmoid', last_layer=True)

    # depthwise softmax (requires keras v2.1+)
    if n_output_channels > 1:
        o = layers.Softmax(axis=-1)(o)

    ### if keras < v2.1, define depthwise softmax as Lambda layer ###
    #def depthwise_softmax(x):
    #    import keras.backend as K
    #    e = K.exp(x - K.max(x, axis=-1, keepdims=True))
    #    s = K.sum(e, axis=-1, keepdims=True)
    #    return e / s
    #o = layers.Lambda(depthwise_softmax)(all_layers[-1])

    print(o)
    return models.Model(inputs=i, outputs=o, name='unet_depth_{}'.format(depth))

#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


def GUNet(n_input_channels,
         n_output_channels,
         depth,
         max_filters,
         h,
         batchnorm=False,
         dropout=0.1,
         residual_blocks=False,
         residual_connection=False):
    '''
    Generate rotationally invariant UNet architecture with specified depth.

    Ref: Ronneberger, Fischer, Brox (2015), U-Net: Convolutional Networks for Biomedical Image Segmentation, MICCAI, LNCS, Vol.9351
    https://arxiv.org/abs/1505.04597

    Args:
    - n_input_channels (int): number of input channels
    - n_output_channels (int): number of output channels
    - depth (int): depth of "U", i.e. network size, measured in number of skip connections
    - max_filters (int): number of filters at bottom layer, power of 2
    - h (str): type of symmetry group, either 'D4' (roto-reflection) or 'C4' (rotations)
    - batchnorm (bool): if true, add batch normalization layer
    - dropout (float): if > 0.0, add dropout layer with this dropout rate
    - residual_blocks (bool): if true, add a residual connection over each convolution block
    - residual_connection (bool): if true, add a residual connection from input to output layer (requires n_input_channels==n_output_channels)

    Returns:
    - keras model
    '''
    
    from keras import layers, models
    from keras_gcnn import layers as glayers

    import keras.backend as K
    def Conv2DReluBatchNorm(x, n_filters, kernel_size, strides, batchnorm=False, dropout=0., name='', activation='relu', last_layer=False):
        shortcut = x

        if d == 0: # first layer transforms from Z2 to C4/D4
            x = glayers.GConv2D(n_filters, kernel_size, h_input='Z2', h_output=h,
                              strides=strides, padding='same',
                              name=name)(x)
        else: # other layers transforms from C4/D4 to C4/D4
            x = glayers.GConv2D(n_filters, kernel_size, h_input=h, h_output=h,
                  strides=strides, padding='same',
                  name=name)(x)

        if batchnorm:
            x = glayers.GBatchNorm(h=h, axis=-1)(x)
            
        x = layers.Activation(activation)(x)
        if dropout > 0:
            x = layers.Dropout(dropout)(x)

        if residual_blocks and not last_layer:
            if K.int_shape(x) != K.int_shape(shortcut): # strides != (1,1):
                # if dimensionality of
                shortcut = layers.Conv2D(n_filters, kernel_size=(1,1), strides=strides, padding='same')(shortcut)
                shortcut = layers.BatchNormalization()(shortcut)

            x = layers.add([shortcut, x])

        return x


    i = layers.Input(shape=(None, None, n_input_channels))

    filters_per_layer_up = [max_filters//(2**i) for i in range(depth+1) ]
    filters_per_layer_down = filters_per_layer_up[::-1]
    filters_per_layer_up.pop() # do not repeat n_filters for bottom layer

    ### downsampling path
    downsampling_layers = []
    for d in range(depth+1):
        if d == 0:
            x = i
            kernel = (3,3)
            strides = (1,1)
        else:
            kernel = (3,3)
            strides = (2,2) # downsampling per stride instead of maxpool
        #print(filters_per_layer_down[d])
        x = Conv2DReluBatchNorm(x, n_filters=filters_per_layer_down[d],
                                kernel_size=kernel, strides=strides,
                                batchnorm=batchnorm, dropout=dropout, name='down_{}'.format(d))
        downsampling_layers.append(x)


    # upsampling path
    bottom_layer = downsampling_layers.pop()
    upsampling_layers = [bottom_layer]

    for d in range(depth):

        u = layers.UpSampling2D(size=(2,2))(upsampling_layers.pop()) # upsample last layer
        c = layers.Concatenate(axis=-1)([u, downsampling_layers.pop()]) # concatenate with corresponding layer in downsampling path
        x = Conv2DReluBatchNorm(c, n_filters=filters_per_layer_up[d],
                                kernel_size=kernel, strides=(1,1), ## STRIDES
                                batchnorm=batchnorm, dropout=dropout, name='up_{}'.format(d))

        upsampling_layers.append(x)

    # output
    o = Conv2DReluBatchNorm(upsampling_layers[-1], n_filters=n_output_channels, kernel_size=(1,1), strides=(1,1), activation='sigmoid', last_layer=True)
    o = glayers.pooling.GroupPool(h_input=h)(o)
    


    if residual_connection:
        if K.int_shape(i) != K.int_shape(o): # strides != (1,1):
            # if dimensionality of
            shortcut = layers.Conv2D(n_output_channels, kernel_size=(1,1), strides=(1,1), padding='same')(i)
            shortcut = layers.BatchNormalization()(shortcut)

        #assert n_input_channels == n_output_channels, "Cannot make residual connection: input and output must have same number of channels."
        o = layers.add([shortcut,o])
        o = Conv2DReluBatchNorm(o, n_filters=n_output_channels, kernel_size=(1,1), strides=(1,1), activation='sigmoid', last_layer=True)

    # depthwise softmax (requires keras v2.1+)
    if n_output_channels > 1:
        o = layers.Softmax(axis=-1)(o)

    ### if keras < v2.1, define depthwise softmax as Lambda layer ###
    #def depthwise_softmax(x):
    #    import keras.backend as K
    #    e = K.exp(x - K.max(x, axis=-1, keepdims=True))
    #    s = K.sum(e, axis=-1, keepdims=True)
    #    return e / s
    #o = layers.Lambda(depthwise_softmax)(all_layers[-1])

    print(o)
    return models.Model(inputs=i, outputs=o, name='unet_depth_{}'.format(depth))

#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

def UNet2(n_input_channels, n_output_channels):
    from keras.layers import Input, Dropout, UpSampling2D, MaxPooling2D, BatchNormalization, Conv2D, Concatenate
    from keras.models import Model

    inputs = Input((None, None, n_input_channels))
    conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = BatchNormalization()(conv1)
    conv1 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    conv1 = BatchNormalization()(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = BatchNormalization()(conv2)
    conv2 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    conv2 = BatchNormalization()(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = BatchNormalization()(conv3)
    conv3 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    conv3 = BatchNormalization()(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = BatchNormalization()(conv4)
    conv4 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    conv4 = BatchNormalization()(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = BatchNormalization()(conv5)
    conv5 = Conv2D(1024, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
    conv5 = BatchNormalization()(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = Conv2D(512, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop5))
    merge6 = Concatenate(axis=-1)([conv4,up6])
    conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = BatchNormalization()(conv6)
    conv6 = Conv2D(512, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)
    conv6 = BatchNormalization()(conv6)

    up7 = Conv2D(256, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv6))
    merge7 = Concatenate(axis=-1)([conv3,up7])
    conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = BatchNormalization()(conv7)
    conv7 = Conv2D(256, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)
    conv7 = BatchNormalization()(conv7)

    up8 = Conv2D(128, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv7))
    merge8 = Concatenate(axis=-1)([conv2,up8])
    conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = BatchNormalization()(conv8)
    conv8 = Conv2D(128, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)
    conv8 = BatchNormalization()(conv8)

    up9 = Conv2D(64, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv8))
    merge9 = Concatenate(axis=-1)([conv1,up9])
    conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = BatchNormalization()(conv9)
    conv9 = Conv2D(64, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = BatchNormalization()(conv9)
    conv9 = Conv2D(2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = BatchNormalization()(conv9)
    conv10 = Conv2D(n_output_channels, 1, activation = 'sigmoid')(conv9)
    
    return Model(inputs = inputs, outputs = conv10)

#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


def GUNet2(n_input_channels, n_output_channels, h='C4'):
    from keras.layers import Input, Dropout, UpSampling2D, MaxPooling2D, Concatenate
    from keras.models import Model
    from keras_gcnn.layers import GConv2D, GBatchNorm
    from keras_gcnn.layers.pooling import GroupPool 

    inputs = Input((None, None, n_input_channels))
    conv1 = GConv2D(64, 3, h_input='Z2', h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
    conv1 = GBatchNorm(h=h)(conv1)
    conv1 = GConv2D(64, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
    conv1 = GBatchNorm(h=h)(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = GConv2D(128, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool1)
    conv2 = GBatchNorm(h=h)(conv2)
    conv2 = GConv2D(128, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
    conv2 = GBatchNorm(h=h)(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = GConv2D(256, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
    conv3 = GBatchNorm(h=h)(conv3)
    conv3 = GConv2D(256, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
    conv3 = GBatchNorm(h=h)(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    conv4 = GConv2D(512, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
    conv4 = GBatchNorm(h=h)(conv4)
    conv4 = GConv2D(512, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
    conv4 = GBatchNorm(h=h)(conv4)
    drop4 = Dropout(0.5)(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

    conv5 = GConv2D(1024, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
    conv5 = GBatchNorm(h=h)(conv5)
    conv5 = GConv2D(1024, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
    conv5 = GBatchNorm(h=h)(conv5)
    drop5 = Dropout(0.5)(conv5)

    up6 = GConv2D(512, 2, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop5))
    merge6 = Concatenate(axis=-1)([conv4,up6])
    conv6 = GConv2D(512, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
    conv6 = GBatchNorm(h=h)(conv6)
    conv6 = GConv2D(512, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)
    conv6 = GBatchNorm(h=h)(conv6)

    up7 = GConv2D(256, 2, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv6))
    merge7 = Concatenate(axis=-1)([conv3,up7])
    conv7 = GConv2D(256, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
    conv7 = GBatchNorm(h=h)(conv7)
    conv7 = GConv2D(256, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)
    conv7 = GBatchNorm(h=h)(conv7)

    up8 = GConv2D(128, 2, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv7))
    merge8 = Concatenate(axis=-1)([conv2,up8])
    conv8 = GConv2D(128, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
    conv8 = GBatchNorm(h=h)(conv8)
    conv8 = GConv2D(128, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)
    conv8 = GBatchNorm(h=h)(conv8)

    up9 = GConv2D(64, 2, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv8))
    merge9 = Concatenate(axis=-1)([conv1,up9])
    conv9 = GConv2D(64, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
    conv9 = GBatchNorm(h=h)(conv9)
    conv9 = GConv2D(64, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = GBatchNorm(h=h)(conv9)
    conv9 = GConv2D(2, 3, h_input=h, h_output=h, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
    conv9 = GBatchNorm(h=h)(conv9)
    conv10 = GroupPool(h_input=h)(conv9)
    conv10 = Conv2D(n_output_channels, 1, activation = 'sigmoid')(conv10)

    return Model(inputs = inputs, outputs = conv10)

